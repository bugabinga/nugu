use palette.nu

def mix [ mode:string, accent: string ] {
  if $mode == "dark" {
    pastel mix --fraction 0.33 $accent |
    pastel lighten 0.1666 |
    pastel desaturate 0.201 |
    pastel format hex |
    str trim
  } else  {
    pastel mix --fraction 0.33 $accent |
    pastel darken 0.0999 |
    pastel desaturate 0.201 |
    pastel format hex |
    str trim 
  }
}

def wezterm_theme [ mode:string ] {
  let nugu = ( palette nugu $mode )

  # partial function application `°_°´
  alias mix_nugu = mix $mode $nugu.palette.content.accent

  let black   = ( "#000000" | mix_nugu )

  let maroon  = ( "#800000" | mix_nugu )
  let green   = ( "#00ff00" | mix_nugu )
  let olive   = ( "#808000" | mix_nugu )

  let navy    = ( "#000080" | mix_nugu )
  let purple  = ( "#800080" | mix_nugu )
  let teal    = ( "#008080" | mix_nugu )
  let silver  = ( "#c0c0c0" | mix_nugu )

  let grey    = ( "#808080" | mix_nugu )
  let red     = ( "#ff0000" | mix_nugu )
  let lime    = ( "#80cc00" | mix_nugu )
  let yellow  = ( "#ffff00" | mix_nugu )

  let blue    = ( "#0000ff" | mix_nugu )
  let fuchsia = ( "#ff00ff" | mix_nugu )
  let aqua    = ( "#00ffff" | mix_nugu )

  let white   = ( "#ffffff" | mix_nugu )

  $"{
    -- The default text color
    foreground = '($nugu.palette.content.normal)',
    -- The default background color
    background = '($nugu.palette.content.backdrop)',

    -- Overrides the cell background color when the current cell is occupied by the
    -- cursor and the cursor style is set to Block
    cursor_bg = '($nugu.palette.ui.accent)',
    -- Overrides the text color when the current cell is occupied by the cursor
    cursor_fg = '($nugu.palette.ui.important_global)',
    -- Specifies the border color of the cursor when the cursor style is set to Block,
    -- or the color of the vertical or horizontal bar when the cursor style is set to
    -- Bar or Underline.
    cursor_border = '($nugu.palette.ui.accent)',

    -- the foreground color of selected text
    selection_fg = '($nugu.palette.content.normal)',
    -- the background color of selected text
    selection_bg = '($nugu.palette.ui.important_local)',

    -- The color of the scrollbar 'thumb'; the portion that represents the current viewport
    scrollbar_thumb = '($nugu.palette.ui.unfocus)',

    -- The color of the split lines between panes
    split = '($nugu.palette.ui.important_global)',

    ansi = {
      '($black)',
      '($maroon)',
      '($green)',
      '($olive)',
      '($navy)',
      '($purple)',
      '($teal)',
      '($silver)',
    },

    brights = {
      '($grey)',
      '($red)',
      '($lime)',
      '($yellow)',
      '($blue)',
      '($fuchsia)',
      '($aqua)',
      '($white)',
    },

    -- Since: 20220319-142410-0fcdea07
    -- When the IME, a dead key or a leader key are being processed and are effectively
    -- holding input pending the result of input composition, change the cursor
    -- to this color to give a visual cue about the compose state.
    compose_cursor = '($nugu.palette.ui.focus)',
    -- Colors for copy_mode and quick_select
    -- available since: 20220807-113146-c2fee766
    -- In copy_mode, the color of the active text is:
    -- 1. copy_mode_active_highlight_* if additional text was selected using the mouse
    -- 2. selection_* otherwise
    copy_mode_active_highlight_bg = { Color = '($nugu.palette.ui.important_global)' },
    copy_mode_active_highlight_fg = { Color = '($nugu.palette.content.normal)' },
    copy_mode_inactive_highlight_bg = { Color = '($nugu.palette.ui.unfocus)' },
    copy_mode_inactive_highlight_fg = { Color = '($nugu.palette.content.minor)' },

    quick_select_label_bg = { Color = '($nugu.palette.ui.important_global)' },
    quick_select_label_fg = { Color = '($nugu.palette.content.normal)' },
    quick_select_match_bg = { Color = '($nugu.palette.ui.important_local)' },
    quick_select_match_fg = { Color = '($nugu.palette.content.normal)' },

    tab_bar = {
        -- The color of the strip that goes along the top of the window
        -- does not apply when fancy tab bar is in use
        background = '($nugu.palette.ui.backdrop)',

        -- The active tab is the one that has focus in the window
        active_tab = {
          -- The color of the background area for the tab
          bg_color = '($nugu.palette.ui.backdrop)',
          -- The color of the text for the tab
          fg_color = '($nugu.palette.ui.important_global)',

          -- Specify whether you want 'Half', 'Normal' or 'Bold' intensity for the
          -- label shown for this tab.
          -- The default is 'Normal'
          intensity = 'Bold',

          -- Specify whether you want 'None', 'Single' or 'Double' underline for
          -- label shown for this tab.
          -- The default is 'None'
          underline = 'Double',

          -- Specify whether you want the text to be italic or not
          -- for this tab.  The default is false.
          italic = false,

          -- Specify whether you want the text to be rendered with strikethrough 
          -- or not for this tab.  The default is false.
          strikethrough = false,
        },

        -- Inactive tabs are the tabs that do not have focus
        inactive_tab = {
          bg_color = '($nugu.palette.ui.backdrop)',
          fg_color = '($nugu.palette.ui.normal)',
          intensity = 'Normal',

          -- The same options that were listed under the `active_tab` section above
          -- can also be used for `inactive_tab`.
        },

        -- You can configure some alternate styling when the mouse pointer
        -- moves over inactive tabs
        inactive_tab_hover = {
          bg_color = '($nugu.palette.ui.focus)',
          fg_color = '($nugu.palette.ui.normal)',
          italic = true,

          -- The same options that were listed under the `active_tab` section above
          -- can also be used for `inactive_tab_hover`.
        },

        -- The new tab button that let you create new tabs
        new_tab = {
          bg_color = '($nugu.palette.ui.backdrop)',
          fg_color = '($nugu.palette.ui.unfocus)',

          -- The same options that were listed under the `active_tab` section above
          -- can also be used for `new_tab`.
        },

        -- You can configure some alternate styling when the mouse pointer
        -- moves over the new tab button
        new_tab_hover = {
          bg_color = '($nugu.palette.ui.focus)',
          fg_color = '($nugu.palette.ui.normal)',
          italic = true,

          -- The same options that were listed under the `active_tab` section above
          -- can also be used for `new_tab_hover`.
        },
      },
  }"
}

export def theme [] {
  $"return {
    ['nugu-dark'] = (wezterm_theme dark),
    ['nugu-light'] = (wezterm_theme light),
  }"
}
