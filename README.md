# nugu

Personal color theme for various applications.

## TODO

* [x] helix
* [x] wezterm
* [ ] LS\_COLORS (vivid?)
* [ ] firefox
* [ ] nushell
* [x] bat ( use ansi theme to apply wezterm colors)
